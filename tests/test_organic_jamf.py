import pytest
import requests.exceptions
import responses
from organic_jamf import (
    ExtensionAttributeInvalid,
    ExtensionAttributeNotFound,
    NotFound,
    OrganicJamf,
    TooMany,
)
from requests.auth import AuthBase
from responses import matchers

MOCK_AUTH_STRING = "This is a MockAuth"
EXPECTED_AUTH = {"Authorization": MOCK_AUTH_STRING}
EXAMPLE_JSS = "https://jss.example.com:8443"


class MockAuth(AuthBase):
    def __call__(self, r):
        r.headers["Authorization"] = MOCK_AUTH_STRING
        return r


@pytest.fixture
def organic_jamf():
    return OrganicJamf(EXAMPLE_JSS, MockAuth())


def jamf_url(endpoint):
    return f"{EXAMPLE_JSS}/{endpoint}"


@responses.activate(assert_all_requests_are_fired=True)
def test_get_mobile_device_detail(organic_jamf):
    """
    Ensures get_mobile_device_detail returns content from the API and uses its
    authorization correctly.
    """
    expected_json = {"test": "test_get_mobile_device_detail"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-devices/1001/detail"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_detail(1001) == expected_json


@responses.activate
def test_get_mobile_device_detail_404(organic_jamf):
    """
    Ensures get_mobile_device_detail raises appropriately when the device is
    not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-devices/1001/detail"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_detail(1001)


@responses.activate
def test_get_mobile_device_500(organic_jamf):
    """
    Ensures get_mobile_device_detail raises appropriately when another non-2xx
    HTTP code is returned
    """

    detail = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-devices/1001/detail"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_detail(1001)


@responses.activate
def test_set_mobile_device_extension_attributes_single(organic_jamf):
    """
    Ensures set_mobile_device_extension_attributes sends the correct data when
    a single updated EA is requested
    """
    expected_json = {"success": True}
    attributes = responses.Response(
        method="PATCH",
        url=jamf_url("api/v2/mobile-devices/1001"),
        match=[
            matchers.json_params_matcher(
                {"updatedExtensionAttributes": [{"name": "color", "value": ["blue"]}]}
            )
        ],
        json=expected_json,
    )
    responses.add(attributes)
    assert (
        organic_jamf.set_mobile_device_extension_attributes(1001, {"color": "blue"})
        == expected_json
    )


@responses.activate
def test_set_mobile_device_extension_attributes_multiple(organic_jamf):
    """
    Ensures set_mobile_device_extension_attributes sends the correct data when
    multiple updated EAs are requested
    """
    expected_json = {"success": True}
    attributes = responses.Response(
        method="PATCH",
        url=jamf_url("api/v2/mobile-devices/1001"),
        match=[
            matchers.json_params_matcher(
                {
                    "updatedExtensionAttributes": [
                        {"name": "color", "value": ["blue"]},
                        {"name": "theme", "value": ["dark"]},
                    ]
                }
            )
        ],
        json=expected_json,
    )
    responses.add(attributes)
    assert (
        organic_jamf.set_mobile_device_extension_attributes(
            1001, {"color": "blue", "theme": "dark"}
        )
        == expected_json
    )


@responses.activate
def test_set_mobile_device_extension_attributes_404(organic_jamf):
    """
    Ensures set_mobile_device_extension_attributes raises appropriately when
    the device is not found.
    """
    attributes = responses.Response(
        method="PATCH", url=jamf_url("api/v2/mobile-devices/1001"), status=404
    )
    responses.add(attributes)
    with pytest.raises(NotFound):
        organic_jamf.set_mobile_device_extension_attributes(
            1001, {"color": "blue", "theme": "dark"}
        )


@responses.activate
def test_set_mobile_device_extension_attributes_500(organic_jamf):
    """
    Ensures set_mobile_device_extension_attributes raises appropriately when
    another non-2xx HTTP code is returned
    """
    attributes = responses.Response(
        method="PATCH", url=jamf_url("api/v2/mobile-devices/1001"), status=500
    )
    responses.add(attributes)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.set_mobile_device_extension_attributes(
            1001, {"color": "blue", "theme": "dark"}
        )


@responses.activate
def test_get_mobile_devices(organic_jamf):
    """
    Ensures get_mobile_devices calls the correct endpoint and returns its data
    """
    expected_json = [{"correct": True}]
    returning_json = {"totalCount": 1, "results": expected_json}
    devices = responses.Response(
        method="GET", url=jamf_url("api/v2/mobile-devices/"), json=returning_json
    )
    responses.add(devices)
    assert organic_jamf.get_mobile_devices() == expected_json


@responses.activate
def test_get_mobile_devices_too_many(organic_jamf):
    """
    Ensures get_mobile_devices raises an error when more devices exist than it
    received
    """
    expected_json = [{"correct": True}]
    returning_json = {"totalCount": 2, "results": expected_json}
    devices = responses.Response(
        method="GET", url=jamf_url("api/v2/mobile-devices/"), json=returning_json
    )
    responses.add(devices)
    with pytest.raises(TooMany):
        organic_jamf.get_mobile_devices()


@responses.activate
def test_get_mobile_devices_500(organic_jamf):
    """
    Ensures get_mobile_devices raises an error when it receives a non-2xx
    response from JPS
    """
    devices = responses.Response(
        method="GET", url=jamf_url("api/v2/mobile-devices/"), status=500
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_devices()


extension_attribute_data = [
    {"name": "color", "value": ["blue"]},
    {"name": "no_value"},
    {"name": "empty_value", "value": []},
    {"name": "multi_value", "value": ["one", "two"]},
    {"name": "theme", "value": ["dark"]},
]


def test_get_extension_attribute(organic_jamf):
    """
    Ensures get_extension_attribute returns the appropriate extension attribute
    given proper data
    """
    assert (
        organic_jamf.get_extension_attribute(extension_attribute_data, "theme")
        == "dark"
    )


def test_get_extension_attribute_not_found(organic_jamf):
    """
    Ensures get_extension_attribute raises properly when the requested
    extension attribute does not exist
    """
    with pytest.raises(ExtensionAttributeNotFound):
        organic_jamf.get_extension_attribute(
            extension_attribute_data, "this one doesn't exist"
        )


def test_get_extension_attribute_invalid(organic_jamf):
    """
    Ensures get_extension_attribute raises properly when the requested
    extension attribute has no value property
    """
    with pytest.raises(ExtensionAttributeInvalid):
        organic_jamf.get_extension_attribute(extension_attribute_data, "no_value")


def test_get_extension_attribute_multiple_values(organic_jamf):
    """
    Ensures get_extension_attribute raises properly when the requested
    extension attribute has more than one value
    """
    with pytest.raises(ExtensionAttributeInvalid):
        organic_jamf.get_extension_attribute(extension_attribute_data, "multi_value")


def test_get_extension_attribute_no_value(organic_jamf):
    """
    Ensures get_extension_attribute raises properly when the requested
    extension attribute has a value property, but it's an empty list
    """
    with pytest.raises(ExtensionAttributeInvalid):
        organic_jamf.get_extension_attribute(extension_attribute_data, "empty_value")


def test_session(organic_jamf):
    """Ensures that OrganicJamf.Session is a requests.Session on a new instance

    Provided to make the API of organic_jamf.session a contract rather than an
    implicitly provided nicety.
    """

    assert type(organic_jamf.session) == requests.Session


def test_changed_mobile_device_extension_attributes():
    """
    Ensures that OrganicJamf.changed_mobile_device_extension_attributes
    functions properly.
    """
    old_attributes = [
        {"name": "Same value", "value": ["correct"]},
        {"name": "Different value", "value": ["not correct"]},
    ]
    new_attributes = {
        "Nonexistent value": "correct",
        "Same value": "correct",
        "Different value": "correct",
    }
    assert OrganicJamf.changed_mobile_device_extension_attributes(
        old_attributes, new_attributes
    ) == {"Nonexistent value": "correct", "Different value": "correct"}


def test_changed_mobile_device_extension_attributes_no_changes():
    """
    Ensures that OrganicJamf.changed_mobile_device_extension_attributes returns
    nothing when all attributes are the same
    """
    old_attributes = [
        {"name": "Same value", "value": ["correct"]},
        {"name": "Different value", "value": ["correct"]},
    ]
    new_attributes = {"Same value": "correct", "Different value": "correct"}
    assert (
        OrganicJamf.changed_mobile_device_extension_attributes(
            old_attributes, new_attributes
        )
        == {}
    )


def test_changed_mobile_device_extension_attributes_no_attributes():
    """
    Ensures that OrganicJamf.changed_mobile_device_extension_attributes returns
    nothing when no attributes are given
    """
    old_attributes = [
        {"name": "Same value", "value": ["correct"]},
        {"name": "Different value", "value": ["correct"]},
    ]
    new_attributes = {}
    assert (
        OrganicJamf.changed_mobile_device_extension_attributes(
            old_attributes, new_attributes
        )
        == {}
    )


@responses.activate
def test_get_advanced_mobile_device_search_id(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_advanced_mobile_device_search_id"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_mobile_device_search(1001) == expected_json


@responses.activate
def test_get_advanced_mobile_device_search_id_404(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_mobile_device_search(1001)


@responses.activate
def test_get_advanced_mobile_device_search_id_500(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_mobile_device_search(1001)


@responses.activate
def test_get_advanced_mobile_device_search_name(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_advanced_mobile_device_search_name"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_mobile_device_search("Test Name") == expected_json


@responses.activate
def test_get_advanced_mobile_device_search_name_404(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_mobile_device_search("Test Name")


@responses.activate
def test_get_advanced_mobile_device_search_name_500(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_mobile_device_search("Test Name")


@responses.activate
def test_get_mobile_device_history(organic_jamf):
    """
    Ensures get_mobile_device_history returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_mobile_device_history"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicehistory/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_history(1001) == expected_json


@responses.activate
def test_get_mobile_device_history_404(organic_jamf):
    """
    Ensures get_mobile_device_history raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicehistory/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_history(1001)


@responses.activate
def test_get_mobile_device_history_500(organic_jamf):
    """
    Ensures get_mobile_device_history raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicehistory/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_history(1001)


@responses.activate
def test_get_mobile_device_group_id(organic_jamf):
    """
    Ensures get_mobile_device_group returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_mobile_device_history"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_group(1001) == expected_json


@responses.activate
def test_get_mobile_device_group_id_404(organic_jamf):
    """
    Ensures get_mobile_device_group raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_group(1001)


@responses.activate
def test_get_mobile_device_group_id_500(organic_jamf):
    """
    Ensures get_mobile_device_group raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_group(1001)


@responses.activate
def test_get_mobile_device_group_name(organic_jamf):
    """
    Ensures get_mobile_device_group returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_mobile_device_history"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_group("Test Name") == expected_json


@responses.activate
def test_get_mobile_device_group_name_404(organic_jamf):
    """
    Ensures get_mobile_device_group raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_group("Test Name")


@responses.activate
def test_get_mobile_device_group_name_500(organic_jamf):
    """
    Ensures get_mobile_device_group raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_group("Test Name")


@responses.activate
def test_get_mobile_device_prestages_scope(organic_jamf):
    """
    Ensures get_mobile_device_prestages_scope calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_mobile_device_prestages_scope"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-device-prestages/scope"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_mobile_device_prestages_scope() == expected_json


@responses.activate
def test_get_mobile_device_prestages_scope_500(organic_jamf):
    """
    Ensures get_mobile_device_prestages_scope raises an error when it receives
    a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET", url=jamf_url("api/v2/mobile-device-prestages/scope"), status=500
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_prestages_scope()


@responses.activate
def test_get_mobile_device_prestage(organic_jamf):
    """
    Ensures get_mobile_device_prestage returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_mobile_device_prestage"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-device-prestages/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_prestage(1001) == expected_json


@responses.activate
def test_get_mobile_device_prestage_404(organic_jamf):
    """
    Ensures get_mobile_device_prestage raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-device-prestages/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_prestage(1001)


@responses.activate
def test_get_mobile_device_prestage_500(organic_jamf):
    """
    Ensures get_mobile_device_prestage raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-device-prestages/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_prestage(1001)


@responses.activate
def test_get_mobile_device_prestages(organic_jamf):
    """
    Ensures get_mobile_device_prestages calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_mobile_device_prestages"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("api/v2/mobile-device-prestages"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_mobile_device_prestages() == expected_json


@responses.activate
def test_get_mobile_device_prestages_500(organic_jamf):
    """
    Ensures get_mobile_device_prestages_scope raises an error when it receives
    a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET", url=jamf_url("api/v2/mobile-device-prestages"), status=500
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_prestages()


@responses.activate
def test_get_advanced_computer_searches(organic_jamf):
    """
    Ensures get_advanced_computer_searches calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_advanced_computer_searches"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_advanced_computer_searches() == expected_json


@responses.activate
def test_get_advanced_computer_searches_500(organic_jamf):
    """
    Ensures get_advanced_computer_searches raises an error when it receives
    a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET", url=jamf_url("JSSResource/advancedcomputersearches"), status=500
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_computer_searches()


@responses.activate
def test_get_advanced_mobile_device_searches(organic_jamf):
    """
    Ensures get_advanced_mobile_device_searches calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_advanced_mobile_device_searches"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("api/v1/advanced-mobile-device-searches"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_advanced_mobile_device_searches() == expected_json


@responses.activate
def test_get_advanced_mobile_device_searches_500(organic_jamf):
    """
    Ensures get_advanced_mobile_device_searches raises an error when it
    receives a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET",
        url=jamf_url("api/v1/advanced-mobile-device-searches"),
        status=500,
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_mobile_device_searches()


@responses.activate
def test_get_advanced_user_searches(organic_jamf):
    """
    Ensures get_advanced_user_searchess calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_advanced_user_searches"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_advanced_user_searches() == expected_json


@responses.activate
def test_get_advanced_user_searches_500(organic_jamf):
    """
    Ensures get_advanced_user_searches raises an error when it
    receives a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches"),
        status=500,
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_user_searches()


@responses.activate
def test_get_computer_groups(organic_jamf):
    """
    Ensures get_computer_groups calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_computer_groups"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_computer_groups() == expected_json


@responses.activate
def test_get_computer_groups_500(organic_jamf):
    """
    Ensures get_computer_groups raises an error when it
    receives a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups"),
        status=500,
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_computer_groups()


@responses.activate
def test_get_mobile_device_groups(organic_jamf):
    """
    Ensures get_mobile_device_groups calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_mobile_device_groups"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_mobile_device_groups() == expected_json


@responses.activate
def test_get_mobile_device_groups_500(organic_jamf):
    """
    Ensures get_mobile_device_groups raises an error when it
    receives a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups"),
        status=500,
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_groups()


@responses.activate
def test_get_user_groups(organic_jamf):
    """
    Ensures get_user_groups calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_user_groups"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_user_groups() == expected_json


@responses.activate
def test_get_user_groups_500(organic_jamf):
    """
    Ensures get_user_groups raises an error when it
    receives a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups"),
        status=500,
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_user_groups()


@responses.activate
def test_get_advanced_computer_search_id(organic_jamf):
    """
    Ensures get_advanced_computer_search returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_advanced_computer_search_id"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_computer_search(1001) == expected_json


@responses.activate
def test_get_advanced_computer_search_id_404(organic_jamf):
    """
    Ensures get_advanced_computer_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_computer_search(1001)


@responses.activate
def test_get_advanced_computer_search_id_500(organic_jamf):
    """
    Ensures get_advanced_computer_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_computer_search(1001)


@responses.activate
def test_get_advanced_computer_search_name(organic_jamf):
    """
    Ensures get_advanced_computer_search returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_advanced_computer_search_name"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_computer_search("Test Name") == expected_json


@responses.activate
def test_get_advanced_computer_search_name_404(organic_jamf):
    """
    Ensures get_advanced_computer_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_computer_search("Test Name")


@responses.activate
def test_get_advanced_computer_search_name_500(organic_jamf):
    """
    Ensures get_advanced_computer_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_computer_search("Test Name")


@responses.activate
def test_get_advanced_computer_search_xml_id(organic_jamf):
    """
    Ensures get_advanced_computer_search_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_computer_search_xml(1001) == expected_xml


@responses.activate
def test_get_advanced_computer_search_xml_id_404(organic_jamf):
    """
    Ensures get_advanced_computer_search_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_computer_search_xml(1001)


@responses.activate
def test_get_advanced_computer_search_xml_id_500(organic_jamf):
    """
    Ensures get_advanced_computer_search_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_computer_search_xml(1001)


@responses.activate
def test_get_advanced_computer_search_xml_name(organic_jamf):
    """
    Ensures get_advanced_computer_search_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_computer_search_xml("Test Name") == expected_xml


@responses.activate
def test_get_advanced_computer_search_xml_name_404(organic_jamf):
    """
    Ensures get_advanced_computer_search_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_computer_search_xml("Test Name")


@responses.activate
def test_get_advanced_computer_search_xml_name_500(organic_jamf):
    """
    Ensures get_advanced_computer_search_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_computer_search_xml("Test Name")


@responses.activate
def test_get_advanced_mobile_device_search_xml_id(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_mobile_device_search_xml(1001) == expected_xml


@responses.activate
def test_get_advanced_mobile_device_search_xml_id_404(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_mobile_device_search_xml(1001)


@responses.activate
def test_get_advanced_mobile_device_search_xml_id_500(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_mobile_device_search_xml(1001)


@responses.activate
def test_get_advanced_mobile_device_search_xml_name(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.get_advanced_mobile_device_search_xml("Test Name") == expected_xml
    )


@responses.activate
def test_get_advanced_mobile_device_search_xml_name_404(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_mobile_device_search_xml("Test Name")


@responses.activate
def test_get_advanced_mobile_device_search_xml_name_500(organic_jamf):
    """
    Ensures get_advanced_mobile_device_search_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_mobile_device_search_xml("Test Name")


@responses.activate
def test_get_advanced_user_search_id(organic_jamf):
    """
    Ensures get_advanced_user_search returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_advanced_user_search_id"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_user_search(1001) == expected_json


@responses.activate
def test_get_advanced_user_search_id_404(organic_jamf):
    """
    Ensures get_advanced_user_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_user_search(1001)


@responses.activate
def test_get_advanced_user_search_id_500(organic_jamf):
    """
    Ensures get_advanced_user_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_user_search(1001)


@responses.activate
def test_get_advanced_user_search_name(organic_jamf):
    """
    Ensures get_advanced_user_search returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_advanced_user_search_name"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_user_search("Test Name") == expected_json


@responses.activate
def test_get_advanced_user_search_name_404(organic_jamf):
    """
    Ensures get_advanced_user_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_user_search("Test Name")


@responses.activate
def test_get_advanced_user_search_name_500(organic_jamf):
    """
    Ensures get_advanced_user_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_user_search("Test Name")


@responses.activate
def test_get_advanced_user_search_xml_id(organic_jamf):
    """
    Ensures get_advanced_user_search_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_user_search_xml(1001) == expected_xml


@responses.activate
def test_get_advanced_user_search_xml_id_404(organic_jamf):
    """
    Ensures get_advanced_user_search_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_user_search_xml(1001)


@responses.activate
def test_get_advanced_user_search_xml_id_500(organic_jamf):
    """
    Ensures get_advanced_user_search_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_user_search_xml(1001)


@responses.activate
def test_get_advanced_user_search_xml_name(organic_jamf):
    """
    Ensures get_advanced_user_search_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_advanced_user_search_xml("Test Name") == expected_xml


@responses.activate
def test_get_advanced_user_search_xml_name_404(organic_jamf):
    """
    Ensures get_advanced_user_search_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_advanced_user_search_xml("Test Name")


@responses.activate
def test_get_advanced_user_search_xml_name_500(organic_jamf):
    """
    Ensures get_advanced_user_search_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_advanced_user_search_xml("Test Name")


@responses.activate
def test_update_advanced_computer_search_id(organic_jamf):
    """
    Ensures update_advanced_computer_search returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.update_advanced_computer_search(1001, "<test />") == expected_xml
    )


@responses.activate
def test_update_advanced_computer_search_id_404(organic_jamf):
    """
    Ensures update_advanced_computer_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_advanced_computer_search(1001, "<test />")


@responses.activate
def test_update_advanced_computer_search_id_500(organic_jamf):
    """
    Ensures update_advanced_computer_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedcomputersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_advanced_computer_search(1001, "<test />")


@responses.activate
def test_update_advanced_computer_search_name(organic_jamf):
    """
    Ensures update_advanced_computer_search returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.update_advanced_computer_search("Test Name", "<test />")
        == expected_xml
    )


@responses.activate
def test_update_advanced_computer_search_name_404(organic_jamf):
    """
    Ensures update_advanced_computer_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_advanced_computer_search("Test Name", "<test />")


@responses.activate
def test_update_advanced_computer_search_name_500(organic_jamf):
    """
    Ensures update_advanced_computer_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedcomputersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_advanced_computer_search("Test Name", "<test />")


@responses.activate
def test_update_advanced_mobile_device_search_id(organic_jamf):
    """
    Ensures update_advanced_mobile_device_search returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.update_advanced_mobile_device_search(1001, "<test />")
        == expected_xml
    )


@responses.activate
def test_update_advanced_mobile_device_search_id_404(organic_jamf):
    """
    Ensures update_advanced_mobile_device_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_advanced_mobile_device_search(1001, "<test />")


@responses.activate
def test_update_advanced_mobile_device_search_id_500(organic_jamf):
    """
    Ensures update_advanced_mobile_device_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_advanced_mobile_device_search(1001, "<test />")


@responses.activate
def test_update_advanced_mobile_device_search_name(organic_jamf):
    """
    Ensures update_advanced_mobile_device_search returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.update_advanced_mobile_device_search("Test Name", "<test />")
        == expected_xml
    )


@responses.activate
def test_update_advanced_mobile_device_search_name_404(organic_jamf):
    """
    Ensures update_advanced_mobile_device_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_advanced_mobile_device_search("Test Name", "<test />")


@responses.activate
def test_update_advanced_mobile_device_search_name_500(organic_jamf):
    """
    Ensures update_advanced_mobile_device_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedmobiledevicesearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_advanced_mobile_device_search("Test Name", "<test />")


@responses.activate
def test_update_advanced_user_search_id(organic_jamf):
    """
    Ensures update_advanced_user_search returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_advanced_user_search(1001, "<test />") == expected_xml


@responses.activate
def test_update_advanced_user_search_id_404(organic_jamf):
    """
    Ensures update_advanced_user_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_advanced_user_search(1001, "<test />")


@responses.activate
def test_update_advanced_user_search_id_500(organic_jamf):
    """
    Ensures update_advanced_user_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedusersearches/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_advanced_user_search(1001, "<test />")


@responses.activate
def test_update_advanced_user_search_name(organic_jamf):
    """
    Ensures update_advanced_user_search returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.update_advanced_user_search("Test Name", "<test />")
        == expected_xml
    )


@responses.activate
def test_update_advanced_user_search_name_404(organic_jamf):
    """
    Ensures update_advanced_user_search raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_advanced_user_search("Test Name", "<test />")


@responses.activate
def test_update_advanced_user_search_name_500(organic_jamf):
    """
    Ensures update_advanced_user_search raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/advancedusersearches/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_advanced_user_search("Test Name", "<test />")


@responses.activate
def test_get_mobile_device_group_xml_id(organic_jamf):
    """
    Ensures get_mobile_device_group_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_group_xml(1001) == expected_xml


@responses.activate
def test_get_mobile_device_group_xml_id_404(organic_jamf):
    """
    Ensures get_mobile_device_group_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_group_xml(1001)


@responses.activate
def test_get_mobile_device_group_xml_id_500(organic_jamf):
    """
    Ensures get_mobile_device_group_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_group_xml(1001)


@responses.activate
def test_get_mobile_device_group_xml_name(organic_jamf):
    """
    Ensures get_mobile_device_group_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_mobile_device_group_xml("Test Name") == expected_xml


@responses.activate
def test_get_mobile_device_group_xml_name_404(organic_jamf):
    """
    Ensures get_mobile_device_group_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_mobile_device_group_xml("Test Name")


@responses.activate
def test_get_mobile_device_group_xml_name_500(organic_jamf):
    """
    Ensures get_mobile_device_group_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_mobile_device_group_xml("Test Name")


@responses.activate
def test_get_computer_group_id(organic_jamf):
    """
    Ensures get_computer_group returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_computer_group"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_computer_group(1001) == expected_json


@responses.activate
def test_get_computer_group_id_404(organic_jamf):
    """
    Ensures get_computer_group raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_computer_group(1001)


@responses.activate
def test_get_computer_group_id_500(organic_jamf):
    """
    Ensures get_computer_group raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_computer_group(1001)


@responses.activate
def test_get_computer_group_name(organic_jamf):
    """
    Ensures get_computer_group returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_computer_history"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_computer_group("Test Name") == expected_json


@responses.activate
def test_get_computer_group_name_404(organic_jamf):
    """
    Ensures get_computer_group raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_computer_group("Test Name")


@responses.activate
def test_get_computer_group_name_500(organic_jamf):
    """
    Ensures get_computer_group raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_computer_group("Test Name")


@responses.activate
def test_get_computer_group_xml_id(organic_jamf):
    """
    Ensures get_computer_group_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_computer_group_xml(1001) == expected_xml


@responses.activate
def test_get_computer_group_xml_id_404(organic_jamf):
    """
    Ensures get_computer_group_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_computer_group_xml(1001)


@responses.activate
def test_get_computer_group_xml_id_500(organic_jamf):
    """
    Ensures get_computer_group_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_computer_group_xml(1001)


@responses.activate
def test_get_computer_group_xml_name(organic_jamf):
    """
    Ensures get_computer_group_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_computer_group_xml("Test Name") == expected_xml


@responses.activate
def test_get_computer_group_xml_name_404(organic_jamf):
    """
    Ensures get_computer_group_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_computer_group_xml("Test Name")


@responses.activate
def test_get_computer_group_xml_name_500(organic_jamf):
    """
    Ensures get_computer_group_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_computer_group_xml("Test Name")


@responses.activate
def test_get_user_group_id(organic_jamf):
    """
    Ensures get_user_group returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_user_group"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_user_group(1001) == expected_json


@responses.activate
def test_get_user_group_id_404(organic_jamf):
    """
    Ensures get_user_group raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_user_group(1001)


@responses.activate
def test_get_user_group_id_500(organic_jamf):
    """
    Ensures get_user_group raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_user_group(1001)


@responses.activate
def test_get_user_group_name(organic_jamf):
    """
    Ensures get_user_group returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_user_history"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_user_group("Test Name") == expected_json


@responses.activate
def test_get_user_group_name_404(organic_jamf):
    """
    Ensures get_user_group raises correctly when a device is not
    found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_user_group("Test Name")


@responses.activate
def test_get_user_group_name_500(organic_jamf):
    """
    Ensures get_user_group raises correctly when a non 2XX
    status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_user_group("Test Name")


@responses.activate
def test_get_user_group_xml_id(organic_jamf):
    """
    Ensures get_user_group_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_user_group_xml(1001) == expected_xml


@responses.activate
def test_get_user_group_xml_id_404(organic_jamf):
    """
    Ensures get_user_group_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_user_group_xml(1001)


@responses.activate
def test_get_user_group_xml_id_500(organic_jamf):
    """
    Ensures get_user_group_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_user_group_xml(1001)


@responses.activate
def test_get_user_group_xml_name(organic_jamf):
    """
    Ensures get_user_group_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_user_group_xml("Test Name") == expected_xml


@responses.activate
def test_get_user_group_xml_name_404(organic_jamf):
    """
    Ensures get_user_group_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_user_group_xml("Test Name")


@responses.activate
def test_get_user_group_xml_name_500(organic_jamf):
    """
    Ensures get_user_group_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_user_group_xml("Test Name")


@responses.activate
def test_update_computer_group_id(organic_jamf):
    """
    Ensures update_computer_group returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_computer_group(1001, "<test />") == expected_xml


@responses.activate
def test_update_computer_group_id_404(organic_jamf):
    """
    Ensures update_computer_group raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_computer_group(1001, "<test />")


@responses.activate
def test_update_computer_group_id_500(organic_jamf):
    """
    Ensures update_computer_group raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/computergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_computer_group(1001, "<test />")


@responses.activate
def test_update_computer_group_name(organic_jamf):
    """
    Ensures update_computer_group returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_computer_group("Test Name", "<test />") == expected_xml


@responses.activate
def test_update_computer_group_name_404(organic_jamf):
    """
    Ensures update_computer_group raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_computer_group("Test Name", "<test />")


@responses.activate
def test_update_computer_group_name_500(organic_jamf):
    """
    Ensures update_computer_group raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/computergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_computer_group("Test Name", "<test />")


@responses.activate
def test_update_user_group_id(organic_jamf):
    """
    Ensures update_user_group returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_user_group(1001, "<test />") == expected_xml


@responses.activate
def test_update_user_group_id_404(organic_jamf):
    """
    Ensures update_user_group raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_user_group(1001, "<test />")


@responses.activate
def test_update_user_group_id_500(organic_jamf):
    """
    Ensures update_user_group raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/usergroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_user_group(1001, "<test />")


@responses.activate
def test_update_user_group_name(organic_jamf):
    """
    Ensures update_user_group returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_user_group("Test Name", "<test />") == expected_xml


@responses.activate
def test_update_user_group_name_404(organic_jamf):
    """
    Ensures update_user_group raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_user_group("Test Name", "<test />")


@responses.activate
def test_update_user_group_name_500(organic_jamf):
    """
    Ensures update_user_group raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/usergroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_user_group("Test Name", "<test />")


@responses.activate
def test_update_mobile_device_group_id(organic_jamf):
    """
    Ensures update_mobile_device_group returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_mobile_device_group(1001, "<test />") == expected_xml


@responses.activate
def test_update_mobile_device_group_id_404(organic_jamf):
    """
    Ensures update_mobile_device_group raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_mobile_device_group(1001, "<test />")


@responses.activate
def test_update_mobile_device_group_id_500(organic_jamf):
    """
    Ensures update_mobile_device_group raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/mobiledevicegroups/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_mobile_device_group(1001, "<test />")


@responses.activate
def test_update_mobile_device_group_name(organic_jamf):
    """
    Ensures update_mobile_device_group returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert (
        organic_jamf.update_mobile_device_group("Test Name", "<test />") == expected_xml
    )


@responses.activate
def test_update_mobile_device_group_name_404(organic_jamf):
    """
    Ensures update_mobile_device_group raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_mobile_device_group("Test Name", "<test />")


@responses.activate
def test_update_mobile_device_group_name_500(organic_jamf):
    """
    Ensures update_mobile_device_group raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/mobiledevicegroups/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_mobile_device_group("Test Name", "<test />")


@responses.activate
def test_get_webhooks(organic_jamf):
    """
    Ensures get_webhooks calls the correct endpoint and
    returns its data
    """
    expected_json = {"test": "test_get_webhooks"}
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks"),
        json=expected_json,
    )
    responses.add(devices)
    assert organic_jamf.get_webhooks() == expected_json


@responses.activate
def test_get_webhooks_500(organic_jamf):
    """
    Ensures get_webhooks raises an error when it
    receives a non-2xx response from JPS
    """
    devices = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks"),
        status=500,
    )
    responses.add(devices)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_webhooks()


@responses.activate
def test_get_webhook_id(organic_jamf):
    """
    Ensures get_webhook returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_webhook_id"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_webhook(1001) == expected_json


@responses.activate
def test_get_webhook_id_404(organic_jamf):
    """
    Ensures get_webhook raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_webhook(1001)


@responses.activate
def test_get_webhook_id_500(organic_jamf):
    """
    Ensures get_webhook raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_webhook(1001)


@responses.activate
def test_get_webhook_name(organic_jamf):
    """
    Ensures get_webhook returns content
    from the API and uses its authorization correctly.
    """
    expected_json = {"test": "test_get_webhook_name"}
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        json=expected_json,
    )
    responses.add(detail)
    assert organic_jamf.get_webhook("Test Name") == expected_json


@responses.activate
def test_get_webhook_name_404(organic_jamf):
    """
    Ensures get_webhook raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_webhook("Test Name")


@responses.activate
def test_get_webhook_name_500(organic_jamf):
    """
    Ensures get_webhook raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_webhook("Test Name")


@responses.activate
def test_get_webhook_xml_id(organic_jamf):
    """
    Ensures get_webhook_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_webhook_xml(1001) == expected_xml


@responses.activate
def test_get_webhook_xml_id_404(organic_jamf):
    """
    Ensures get_webhook_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_webhook_xml(1001)


@responses.activate
def test_get_webhook_xml_id_500(organic_jamf):
    """
    Ensures get_webhook_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_webhook_xml(1001)


@responses.activate
def test_get_webhook_xml_name(organic_jamf):
    """
    Ensures get_webhook_xml returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test>test</test>"
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.get_webhook_xml("Test Name") == expected_xml


@responses.activate
def test_get_webhook_xml_name_404(organic_jamf):
    """
    Ensures get_webhook_xml raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.get_webhook_xml("Test Name")


@responses.activate
def test_get_webhook_xml_name_500(organic_jamf):
    """
    Ensures get_webhook_xml raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="GET",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.get_webhook_xml("Test Name")


@responses.activate
def test_update_webhook_id(organic_jamf):
    """
    Ensures update_webhook returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_webhook(1001, "<test />") == expected_xml


@responses.activate
def test_update_webhook_id_404(organic_jamf):
    """
    Ensures update_webhook raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_webhook(1001, "<test />")


@responses.activate
def test_update_webhook_id_500(organic_jamf):
    """
    Ensures update_webhook raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/webhooks/id/1001"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_webhook(1001, "<test />")


@responses.activate
def test_update_webhook_name(organic_jamf):
    """
    Ensures update_webhook returns content
    from the API and uses its authorization correctly.
    """
    expected_xml = "<test />"
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        body=expected_xml,
    )
    responses.add(detail)
    assert organic_jamf.update_webhook("Test Name", "<test />") == expected_xml


@responses.activate
def test_update_webhook_name_404(organic_jamf):
    """
    Ensures update_webhook raises correctly
    when an advanced mobile search is not found.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=404,
    )
    responses.add(detail)
    with pytest.raises(NotFound):
        organic_jamf.update_webhook("Test Name", "<test />")


@responses.activate
def test_update_webhook_name_500(organic_jamf):
    """
    Ensures update_webhook raises correctly
    when a non 2XX status is raised.
    """
    detail = responses.Response(
        method="PUT",
        url=jamf_url("JSSResource/webhooks/name/Test Name"),
        match=[matchers.header_matcher(EXPECTED_AUTH)],
        status=500,
    )
    responses.add(detail)
    with pytest.raises(requests.exceptions.HTTPError):
        organic_jamf.update_webhook("Test Name", "<test />")
