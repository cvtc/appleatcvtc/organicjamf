import requests
from urllib.parse import quote

VERSION = "1.15.1"


class OrganicJamf:
    """A limited API wrapper for the Jamf Pro Server

    :param base_url: URL of the JPS. For example, https://jamf.example.com:8443

    :param auth:
        Provide an object that will be used as the authorization attribute
        (auth=) for all requests from this instance. We recommend a JamfAuth
        instance.
    """

    session: requests.Session

    def __init__(self, base_url, auth):
        self.session = requests.Session()
        self.session.auth = auth
        self.base_url = base_url
        self.session.headers.update(
            {
                "Accept": "application/json",
                "Content-type": "application/json",
            }
        )
        self.permissions = {}

    @classmethod
    def _raise_recognized_errors(self, r: requests.Response):
        if r.status_code == 404:
            raise NotFound(
                "The requested resource did not exist, Jamf's response code was 404."
            )

    @classmethod
    def _id_or_name(self, endpoint):
        try:
            int(endpoint)
            return "id"
        except ValueError:
            return "name"

    def _add_permissions(self, permission, permission_type):
        self.permissions.setdefault(permission, [])
        if permission_type not in self.permissions[permission]:
            self.permissions[permission].append(permission_type)

        return self.permissions

    def get_mobile_device_detail(self, mobile_device_id):
        """
        Returns a mobile device dictionary for the device with the given ID.

        :raises NotFound:
            The requested mobile device ID was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """

        r = self.session.get(
            f"{self.base_url}/api/v2/mobile-devices/{mobile_device_id}/detail",
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Mobile Devices", "Read")

        return r.json()

    @classmethod
    def changed_mobile_device_extension_attributes(
        cls, current_extension_attributes: dict, new_extension_attributes: dict
    ) -> dict:
        """
        Returns the values in new_extension_attributes that differ from those
        found in current_extension_attributes.

        :param current_extension_attributes:
            The list contained in the extensionAttributes property of a
            JPS mobile device detail record. This is the "old" side of the
            diff.

        :param new_extension_attributes:
            Dictionary containing the extension attributes to check as
            {"attribute name": "newvalue"}. This is the "new" side of the diff.

        :returns:
            Dictionary in the format {"attribute name": "newvalue"} which
            contains the "new" attributes that meet any of the requirements:
            * The "new" attribute does not appear in the "old" attributes
            * The "new" attribute has a different value than the same attribute
              in the "old" attributes.
            Deletions are not considered: for example, passing in an empty dict
            for new_extension_attributes will always result in an empty dict.
        """
        different_attributes = {}
        for new_attribute_name, new_attribute_value in new_extension_attributes.items():
            try:
                old_attribute_value = cls.get_extension_attribute(
                    current_extension_attributes, new_attribute_name
                )
            except ExtensionAttributeNotFound:
                different_attributes[new_attribute_name] = new_attribute_value
                continue

            if old_attribute_value == new_attribute_value:
                continue

            different_attributes[new_attribute_name] = new_attribute_value

        return different_attributes

    def set_mobile_device_extension_attributes(
        self, mobile_device_id: int, attributes: dict
    ):
        """Sets an extension attribute on a mobile device.

        :param mobile_device_id: JAMF API ID of the mobile device to modify.

        :param attributes:
            Dictionary containing the extension attributes to update as
            {"attribute name": "newvalue"}

        :returns: Updated device dictionary

        :raises NotFound:
            The requested mobile device ID was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """

        new_json = {"updatedExtensionAttributes": []}

        for attribute_name, new_value in attributes.items():
            new_json["updatedExtensionAttributes"].append(
                {"name": attribute_name, "value": [new_value]}
            )

        r = self.session.patch(
            f"{self.base_url}/api/v2/mobile-devices/{mobile_device_id}",
            json=new_json,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Mobile Devices", "Update")
        self.permissions = self._add_permissions("Users", "Update")

        return r.json()

    def get_mobile_devices(self):
        """Returns all mobile devices in the JPS

        Warning: This does not work if your JPS has more than 99999999 devices
        or has a limited page-size. Instead, it will only return as many
        devices as are in the page size.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.

        :raises TooMany:
            The Jamf server has more devices than it returned in this request,
            therefore we are unable to return a full list to the consumer.
            Fixing this requires implementing pagination in Organic Jamf.
        """

        r = self.session.get(
            f"{self.base_url}/api/v2/mobile-devices/",
            params={"page-size": 99999999},
        )
        r.raise_for_status()
        returned = r.json()
        results = returned["results"]
        total = returned["totalCount"]
        if len(results) < total:
            raise TooMany()
        self.permissions = self._add_permissions("Mobile Devices", "Read")

        return results

    @classmethod
    def get_extension_attribute(
        cls, extension_attributes: list, extension_attribute_name: str
    ):
        """Returns the value of the given extension attribute

        Multi-value extension attributes are usually stored as newline
        (\\n)-delimited strings.

        :param extension_attributes:
            The dictionary contained in the extensionAttributes property of a
            JPS mobile device detail record.

        :param extension_attribute_name:
            The name of the extension attribute which should be found and its
            value returned. This is case sensitive.

        :raises ExtensionAttributeNotFound:
            The named extension attribute was not found inside the given
            extension_attributes list.

        :raises ExtensionAttributeInvalid:
            The extension attribute has no "value" property from the Jamf
            server, the "value" property was an empty list, or the "value"
            property's list contained multiple values. Please report this as a
            bug in Organic Jamf.
        """

        try:
            extension_attribute = [
                attribute
                for attribute in extension_attributes
                if attribute["name"] == extension_attribute_name
            ][0]
        except IndexError as e:
            raise ExtensionAttributeNotFound(extension_attribute_name) from e

        try:
            values = extension_attribute["value"]
        except KeyError as e:
            raise ExtensionAttributeInvalid(
                extension_attribute_name
                + " is missing its 'value' list. This should not be possible."
            ) from e

        if len(values) > 1:
            raise ExtensionAttributeInvalid(
                f"{extension_attribute_name} had more than one value: {values}"
            )
        if len(values) < 1:
            raise ExtensionAttributeInvalid(extension_attribute_name)

        return values[0]

    def get_advanced_mobile_device_search(self, id_or_name):
        """
        Returns an advanced mobile device search contents dictionary for
        the advanced search with the given identifier.

        :param id_or_name:
            ID or name of the advanced mobile device search.

        :raises NotFound:
            The requested advanced search ID was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/advancedmobiledevicesearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Advanced Mobile Device Searches", "Read"
        )

        return r.json()

    def get_mobile_device_history(self, mobile_device_id):
        """
        Returns mobile device history contents dictionary for the mobile device
        with the given id.

        :raises NotFound:
            The requested mobile device ID was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(
            f"{self.base_url}/JSSResource/mobiledevicehistory/id/{mobile_device_id}"
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Mobile Devices", "Read")

        return r.json()

    def get_mobile_device_group(self, id_or_name):
        """
        Returns a mobile device group contents dictionary for the mobile device
        group with the given id.

        :param id_or_name:
            The ID or name of the mobile device group.

        :raises NotFound:
            The requested mobile device group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/mobiledevicegroups/"
                f"{endpoint_type}/{id_or_name}"
            )
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Mobile Device Groups", "Read")
        self.permissions = self._add_permissions("Static Mobile Device Groups", "Read")

        return r.json()

    def get_mobile_device_prestages_scope(self):
        """
        Returns all PreStages with device scopes in the JPS.

        :raises NotFound:
            The requested mobile device group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/api/v2/mobile-device-prestages/scope")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Mobile Device PreStage Enrollments", "Read"
        )

        return r.json()

    def get_mobile_device_prestage(self, mobile_prestage_id):
        """
        Retrieve a Mobile Device Prestage with the supplied id

        :raises NotFound:
            The requested mobile device group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(
            f"{self.base_url}/api/v2/mobile-device-prestages/{mobile_prestage_id}"
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Mobile Device PreStage Enrollments", "Read"
        )

        return r.json()

    def get_mobile_device_prestages(self, page=0, page_size=100, req_sort="id:desc"):
        """
        Returns all PreStages in the JPS.

        :param page:
            Page of values to return

        :param page_size:
            How many items to return in each request

        :param sort:
            The value to sort by followed by asc or desc for order.

        :raises NotFound:
            The requested mobile device group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(
            (
                f"{self.base_url}/api/v2/mobile-device-prestages"
                f"?page={page}&page-size={page_size}&sort={quote(req_sort)}"
            )
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Mobile Device PreStage Enrollments", "Read"
        )

        return r.json()

    def get_advanced_computer_searches(self):
        """
        Returns all advanced computer searches.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/JSSResource/advancedcomputersearches")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced Computer Searches", "Read")

        return r.json()

    def get_advanced_mobile_device_searches(self):
        """
        Returns all advanced mobile device searches.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/api/v1/advanced-mobile-device-searches")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Advanced Mobile Device Searches", "Read"
        )

        return r.json()

    def get_advanced_user_searches(self):
        """
        Returns all advanced user searches.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/JSSResource/advancedusersearches")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced User Searches", "Read")

        return r.json()

    def get_computer_groups(self):
        """
        Returns all computer groups.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/JSSResource/computergroups")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Computer Groups", "Read")
        self.permissions = self._add_permissions("Static Computer Groups", "Read")

        return r.json()

    def get_mobile_device_groups(self):
        """
        Returns all mobile device groups.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/JSSResource/mobiledevicegroups")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Mobile Device Groups", "Read")
        self.permissions = self._add_permissions("Static Mobile Device Groups", "Read")

        return r.json()

    def get_user_groups(self):
        """
        Returns all mobile device groups.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/JSSResource/usergroups")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart User Groups", "Read")
        self.permissions = self._add_permissions("Static User Groups", "Read")

        return r.json()

    def get_advanced_computer_search(self, id_or_name):
        """
        Returns an advanced computer search contents dictionary for
        the advanced search with the given identifier.

        :param id_or_name:
            ID or name of the advanced computer search.

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/advancedcomputersearches/"
                f"{endpoint_type}/{id_or_name}"
            )
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced Computer Searches", "Read")

        return r.json()

    def get_advanced_computer_search_xml(self, id_or_name):
        """
        Returns an advanced computer search XML contents for
        the advanced search with the given identifier.

        :param id_or_name:
            ID or name of the advanced computer search.

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"accept": "application/xml"}
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/advancedcomputersearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced Computer Searches", "Read")

        return r.text

    def get_advanced_mobile_device_search_xml(self, id_or_name):
        """
        Returns an advanced mobile device search contents dictionary for
        the advanced search with the given identifier.

        :param id_or_name:
            ID or name of the advanced mobile device search.

        :raises NotFound:
            The requested advanced search ID was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"accept": "application/xml"}
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/advancedmobiledevicesearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Advanced Mobile Device Searches", "Read"
        )

        return r.text

    def get_advanced_user_search(self, id_or_name):
        """
        Returns an advanced user search contents dictionary for
        the advanced search with the given identifier.

        :param id_or_name:
            ID or name of the advanced user search.

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/advancedusersearches/"
                f"{endpoint_type}/{id_or_name}"
            )
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced User Searches", "Read")

        return r.json()

    def get_advanced_user_search_xml(self, id_or_name):
        """
        Returns an advanced user search XML contents for the advanced search
        with the given identifier.

        :param id_or_name:
            ID or name of the advanced user search.

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"Accept": "application/xml"}
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/advancedusersearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced User Searches", "Read")

        return r.text

    def update_advanced_computer_search(self, id_or_name, xml_data: str):
        """
        Returns an advanced computer search XML contents for the advanced
        search with the given identifier.

        :param id_or_name:
            ID or name of the advanced computer search.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (
                f"{self.base_url}/JSSResource/advancedcomputersearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced Computer Searches", "Update")

        return r.text

    def update_advanced_mobile_device_search(self, id_or_name, xml_data: str):
        """
        Returns an advanced mobile device search XML contents for
        the advanced search with the given identifier.

        :param id_or_name:
            ID or name of the advanced mobile device search.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (
                f"{self.base_url}/JSSResource/advancedmobiledevicesearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions(
            "Advanced Mobile Device Searches", "Update"
        )

        return r.text

    def update_advanced_user_search(self, id_or_name, xml_data: str):
        """
        Returns an advanced user search XML contents for the advanced search
        with the given identifier.

        :param id_or_name:
            ID or name of the advanced user search.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (
                f"{self.base_url}/JSSResource/advancedusersearches/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Advanced User Searches", "Update")

        return r.text

    def get_mobile_device_group_xml(self, id_or_name):
        """
        Returns a mobile device group contents xml string for the mobile device
        group with the given id or name.

        :param id_or_name:
            The ID or name of the mobile device group.

        :raises NotFound:
            The requested mobile device group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"accept": "application/xml"}
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/mobiledevicegroups/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Mobile Device Groups", "Read")
        self.permissions = self._add_permissions("Static Mobile Device Groups", "Read")

        return r.text

    def get_computer_group(self, id_or_name):
        """
        Returns a computer group contents dictionary for the computer
        group with the given id.

        :param id_or_name:
            The ID or name of the computer group.

        :raises NotFound:
            The requested computer group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/computergroups/"
                f"{endpoint_type}/{id_or_name}"
            )
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Computer Groups", "Read")
        self.permissions = self._add_permissions("Static Computer Groups", "Read")

        return r.json()

    def get_computer_group_xml(self, id_or_name):
        """
        Returns a computer group contents xml string for the computer
        group with the given id.

        :param id_or_name:
            The ID or name of the computer group.

        :raises NotFound:
            The requested computer group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"accept": "application/xml"}
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/computergroups/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Computer Groups", "Read")
        self.permissions = self._add_permissions("Static Computer Groups", "Read")

        return r.text

    def get_user_group(self, id_or_name):
        """
        Returns a user group contents dictionary for the user group with the
        given id.

        :param id_or_name:
            The ID or name of the user group.

        :raises NotFound:
            The requested user group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (f"{self.base_url}/JSSResource/usergroups/" f"{endpoint_type}/{id_or_name}")
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart User Groups", "Read")
        self.permissions = self._add_permissions("Static User Groups", "Read")

        return r.json()

    def get_user_group_xml(self, id_or_name):
        """
        Returns a user group contents xml string for the user group with the
        given id.

        :param id_or_name:
            The ID or name of the user group.

        :raises NotFound:
            The requested user group ID was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"accept": "application/xml"}
        r = self.session.get(
            (
                f"{self.base_url}/JSSResource/usergroups/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart User Groups", "Read")
        self.permissions = self._add_permissions("Static User Groups", "Read")

        return r.text

    def update_computer_group(self, id_or_name, xml_data: str):
        """
        Updates a computer groups contents for the group with the given
        identifier.

        :param id_or_name:
            ID or name of the computer group.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested group was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (
                f"{self.base_url}/JSSResource/computergroups/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Computer Groups", "Update")
        self.permissions = self._add_permissions("Static Computer Groups", "Update")

        return r.text

    def update_user_group(self, id_or_name, xml_data: str):
        """
        Updates a user groups contents for the group with the given identifier.

        :param id_or_name:
            ID or name of the user group.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested group was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (
                f"{self.base_url}/JSSResource/usergroups/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart User Groups", "Update")
        self.permissions = self._add_permissions("Static User Groups", "Update")

        return r.text

    def update_mobile_device_group(self, id_or_name, xml_data: str):
        """
        Updates a mobile device groups contents for the group with the given
        identifier.

        :param id_or_name:
            ID or name of the mobile device group.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested group was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (
                f"{self.base_url}/JSSResource/mobiledevicegroups/"
                f"{endpoint_type}/{id_or_name}"
            ),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Smart Mobile Device Groups", "Update")
        self.permissions = self._add_permissions("Static Mobile Groups", "Update")

        return r.text

    def get_webhooks(self):
        """
        Returns all webhooks.

        :raises requests.exceptions.HTTPError:
            Any non-2xx status code was returned by the Jamf server.
        """
        r = self.session.get(f"{self.base_url}/JSSResource/webhooks")
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Webhooks", "Read")

        return r.json()

    def get_webhook(self, id_or_name):
        """
        Returns a webhook contents dictionary for the mobile device
        group with the given id or name.

        :param id_or_name:
            The ID or name of the webhook.

        :raises NotFound:
            The requested webhook ID or name was not found by the Jamf
            server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.get(
            (f"{self.base_url}/JSSResource/webhooks/" f"{endpoint_type}/{id_or_name}")
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Webhooks", "Read")

        return r.json()

    def get_webhook_xml(self, id_or_name):
        """
        Returns XML contents for the webhook with the given identifier.

        :param id_or_name:
            ID or name of the webhook.

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        headers = {"Accept": "application/xml"}
        r = self.session.get(
            (f"{self.base_url}/JSSResource/webhooks/" f"{endpoint_type}/{id_or_name}"),
            headers=headers,
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Webhooks", "Read")

        return r.text

    def update_webhook(self, id_or_name, xml_data: str):
        """
        Updates a JPS webhook using XML.

        :param id_or_name:
            ID or name of the webhook.

        :param xml_data:
            XML request body that will be sent to the endpoint

        :raises NotFound:
            The requested advanced search was not found by the Jamf server.

        :raises requests.exceptions.HTTPError:
            Any other non-2xx status code was returned by the Jamf server.
        """
        headers = {"Content-type": "application/xml"}
        endpoint_type = self._id_or_name(id_or_name)
        if endpoint_type == "name":
            id_or_name = quote(id_or_name)
        r = self.session.put(
            (f"{self.base_url}/JSSResource/webhooks/" f"{endpoint_type}/{id_or_name}"),
            headers=headers,
            data=xml_data.encode("utf-8"),
        )
        self._raise_recognized_errors(r)
        r.raise_for_status()
        self.permissions = self._add_permissions("Webhooks", "Update")

        return r.text


class ExtensionAttributeNotFound(Exception):
    """
    The device record does not contain an extension attribute with the given
    name.
    """


class ExtensionAttributeInvalid(Exception):
    """The given extension attribute had a value that should not exist."""


class NotFound(Exception):
    """
    The requested record could not be found. JPS returned a 404 response.
    """


class TooMany(Exception):
    """
    JPS reported that it has more records than it sent. OrganicJamf needs
    pagination implemented to handle this case. This is a bug in OrganicJamf.
    """
