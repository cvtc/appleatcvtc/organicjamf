# Organic Jamf

No longer maintained, please use [jps-api-wrapper](https://gitlab.com/cvtc/appleatcvtc/jps-api-wrapper) instead.

Organic Jamf is a helper for working with Jamf Pro Server in Python. It is not a well-designed API wrapper because that would take too long to write, test, and debug. It helped us reduce copied code in our Jamf automation, though.

## Install

To install Organic Jamf, run the following:

```sh
pip install organic-jamf --extra-index-url https://gitlab.com/api/v4/projects/38973512/packages/pypi/simple
```

Or, place the following in your Pipfile:

```toml
[[source]]
url = "https://gitlab.com/api/v4/projects/38973512/packages/pypi/simple"
verify_ssl = true
name = "organic_jamf"

[packages]
organic-jamf = {version="*", index="organic_jamf"}
```

## Use

We recommend using [Jamf Auth](https://gitlab.com/cvtc/appleatcvtc/jamfauth) to simplify your API authorization experience.

```py3
from jamf_auth import JamfAuth
from organic_jamf import OrganicJamf

base_url = "https://jss.example.com:8443"

with JamfAuth(base_url, username, password) as jamfauth:
    adapter = OrganicJamf(base_url, jamfauth)
    print(adapter.get_mobile_devices())

>>> [{'id': '1', 'name': 'iPad', 'model': 'iPad 5th Generation (Wi-Fi)', ...} ...]
```

We do not currently build documentation for Organic Jamf, but the API is fully documented in the [src/organic_jamf.py](src/organic_jamf.py) file.

## Develop

To get started developing Organic Jamf, clone this repository:

```
git clone https://gitlab.com/cvtc/appleatcvtc/organicjamf.git
cd organicjamf
```

This repository contains a Pipfile for easy management of virtual environments
with Pipenv. Run it, but don't create a lockfile, to install the development
dependencies:

```
pipenv install --skip-lock --dev
```

To run the tests and get coverage information, run:

```
pipenv run pytest --cov=src/ --cov-branch --cov-report=xml --cov-report=term
```

We format files with Black prior to committing. Black is installed in your
Pipenv virtual environment. Run it like this before you commit:

```
pipenv run black .
```

## Copyright

Organic Jamf is Copyright 2022 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.
